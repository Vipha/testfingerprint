package com.aos.test;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

public class ViewAnimation extends LinearLayout {

    private int interval = 500;
    private boolean flash;

    public ViewAnimation(Context context) {
        super(context);
        resetAnimation();
    }

    public ViewAnimation(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        resetAnimation();
    }

    public void resetAnimation() {
        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(getInterval());

        final Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(getInterval());
        fadeOut.setDuration(getInterval());
        startFadeAnimation(fadeIn);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(isFlash()) startFadeAnimation(fadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startFadeAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public boolean isFlash() {
        return flash;
    }

    public void  setFlash(boolean flash) {
        this.flash = flash;
        if(!flash){
            this.setVisibility(VISIBLE);
        }
    }


    private void startFadeAnimation(Animation anim){
        this.startAnimation(anim);
    }
}
