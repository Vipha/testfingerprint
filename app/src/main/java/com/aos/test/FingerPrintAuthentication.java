package com.aos.test;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricManager;

import java.util.Objects;

public class FingerPrintAuthentication extends Dialog {

    private OnAuthListener onAuthListener;
   // private FingerAuth fingerAuth;
    private ImageView iv_finger;
    private TextView tv_close,tv_msg_auth;
    private ViewAnimation view_anim;
    private FingerAuth fingerAuth;


    public FingerPrintAuthentication(@NonNull Context context) {
        super(context);
        Objects.requireNonNull(super.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        super.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        super.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        super.setCanceledOnTouchOutside(false);
        super.setContentView(R.layout.comm_alert_fingerprint_dialog);
        super.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //super.getWindow().setDimAmount(0.9f);
        super.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
        initView();

        BiometricManager biometricManager = BiometricManager.from(context);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("+>>","App can authenticate using biometrics.");
                initFingerAuth(context);
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("+>>","No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("+>>","Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.e("+>>","The user hasn't associated any biometric credentials " +
                        "with their account.");
                break;
        }


    }
    private void initFingerAuth(@NonNull Context context) {
        fingerAuth = new FingerAuth(context);
        fingerAuth.setMaxFailedCount(5);
        fingerAuth.setOnFingerAuthListener(new FingerAuth.OnFingerAuthListener() {
            @Override
            public void onSuccess() {
                iv_finger.setImageResource(R.drawable.ic_finger_print_success);
                tv_msg_auth.setText("Fingerprint recognized");
                tv_msg_auth.setTextColor(Color.parseColor("#8ECE58"));
                view_anim.setFlash(false);
                view_anim.resetAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                        onAuthListener.onSuccess();
                    }
                }, 500);
            }

            @Override
            public void onFailure() {
                iv_finger.setImageResource(R.drawable.ic_finger_print_failed);
                tv_msg_auth.setText("Fingerprint doesn't recognized");
                tv_msg_auth.setTextColor(Color.parseColor("#AF0000"));
                view_anim.setFlash(true);
                view_anim.setInterval(50);
                view_anim.resetAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view_anim.setFlash(false);
                        view_anim.setInterval(500);
                        view_anim.resetAnimation();
                        onAuthListener.onFailure();
                    }
                }, 1500);
            }

            @Override
            public void onError() {
                tv_msg_auth.setText("Fingerprint doesn't recognized you!!");
                tv_msg_auth.setTextColor(Color.parseColor("#AF0000"));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                        onAuthListener.onError();
                    }
                }, 1500);
            }
        });
    }


    private void initView() {
        iv_finger = findViewById(R.id.iv_finger);
        tv_close = findViewById(R.id.tv_close);
        view_anim = findViewById(R.id.view_anim);
        tv_msg_auth = findViewById(R.id.tv_msg_auth);
        tv_msg_auth.setText("Touch the fingerprint sensor");
        tv_msg_auth.setTextColor(Color.parseColor("#837C7C"));
        view_anim.setFlash(true);
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


    @Override
    public void show() {
        super.show();
    }

    public FingerPrintAuthentication setOnAuthListener(OnAuthListener onFingerAuthListener){
        this.onAuthListener = onFingerAuthListener;
        return this;
    }



    public interface OnAuthListener{
        void onSuccess();
        void onError();
        void onFailure();
    }

}
