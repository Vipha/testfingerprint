package com.aos.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends FragmentActivity {

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button = findViewById(R.id.btnScan);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FingerPrintAuthentication fingerPrintAuthentication = new FingerPrintAuthentication(Main2Activity.this);
                fingerPrintAuthentication.setOnAuthListener(new FingerPrintAuthentication.OnAuthListener() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }

                    @Override
                    public void onFailure() {

                    }
                });
                fingerPrintAuthentication.show();
            }
        });
    }
}
